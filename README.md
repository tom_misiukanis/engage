# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Very simple form to add unique (by email) contacts to a MySQL DB. It's built using the SlimPHP framework allowing us to
easily use it's bootstrapping and MVC structure as well as install any dependencies with composer.

Along with Slim this application also uses:

* robmorgan/phinx (DB migrations)
* slim/php-view" (php view rendoring)
* davidepastore/slim-validation (form validation)
* slim/flash (session flash)

### How do I get set up? ###

* Configure your web server's document / web root to be the  public directory. The index.php in this directory serves as the front controller for all HTTP requests entering your application
* DB connection details can be found in phinx.yml. Create DB with these user credentials or update this file and config/settings.php if using different user details
* SSH to project root directory and run composer install and php vendor/bin/phinx migrate
* Example can be found here: [https://engage.misiukanis.me/] (https://engage.misiukanis.me/)

### Other considerations/ran out of time ###

* Repopulate form with inputted values in event of an error
* Implement CSRF to prevent malicious sign up attempts
* Implement Recaptcha
* Email activation
* Email notification of sign up

### Who do I talk to? ###

* Tom Misiukanis (tommis.tm@gmail.com)