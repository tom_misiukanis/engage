<?php
/**
 * Set the validation rules
 */
use Respect\Validation\Validator as v;

//Create the validators
$nameValidator = v::notEmpty();
$emailValidator = v::notEmpty()->email();
$dobValidator = v::date();
$validators = array(
	'name' => $nameValidator,
	'email' => $emailValidator,
	'dob' => $dobValidator
);
