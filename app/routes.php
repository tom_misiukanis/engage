<?php 

// Define app routes

// Default Home Route
$app->get('/', function ($request, $response) {
    
    // Create an array of data used in the view
	$data['view'] = 'home';
	$data['meta_title'] = 'Welcome to the best signup form in the world!';
	$data['flash'] = $this->flash->getMessages();

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $data);
});

// Sign up form post route
$app->post('/', function ($request, $response) {

	// Get the input values
	$data = $request->getParsedBody();

	// Validate the post input
	if($request->getAttribute('has_errors')){
		// There are errors, read them
		$errors = $request->getAttribute('errors');

		// Add to flash session
		$this->flash->addMessage('errors', $errors);

		// Reload the page with errors
		return $response->withStatus(302)->withHeader('Location', '/');
	}
	else{
		// We've passed the first set of validation

		// Sanitize the input data, you can't trust anyone!
		$input['name'] = filter_var($data['name'], FILTER_SANITIZE_STRING);
		$input['email'] = filter_var($data['email'], FILTER_SANITIZE_STRING);
		$input['tel'] = filter_var($data['telephone'], FILTER_SANITIZE_STRING);
		$input['dob'] = filter_var($data['dob'], FILTER_SANITIZE_STRING);

		// Check to see if the address already exists in the DB
		$contact = new Contact($this->db);

		// If it's already there let's start again....
		if($contact->checkExists($input['email']) == TRUE)
		{
			// Manually set the error message
			$errors = [
				'Email' => [
					0 => 'Address Already Exists'
				]
			];
			
			//Return the error message
			$this->flash->addMessage('errors', $errors);

			// Reload the page with errors
			return $response->withStatus(302)->withHeader('Location', '/');

		}
		else {
			// Everything is a ok

			// Save the record to the DB
			$contact->save($input);

			// Create an array of data used in the view
			$data['view'] = 'success';
			$data['meta_title'] = 'Record added!';

			// Rendor the index view
			return $this->renderer->render($response, 'index.phtml', $data);
		}
	}

})->add(new \DavidePastore\Slim\Validation\Validation($validators));;