<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

//Load custom classes
spl_autoload_register(function () {
    require ("../classes/Contact.php");
});

session_start();

// Create and configure Slim app
// Get settings from config
$settings = require __DIR__ . '/../config/settings.php';

$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../app/dependencies.php';

// Validation Rules
require __DIR__ . '/../app/validation.php';

// Register routes
require __DIR__ . '/../app/routes.php';

// Run app
$app->run();