<?php

// Very simple class to check and save data
// Usually we'd have full a full CRUD
Class Contact {

	// We need to work with the DB instance we created
	public function __construct($db)
	{
		$this->db = $db;
	}

	// Very simple method to check email doesn't already exist in the DB
    public function checkExists($email)
    {

    	$sql = "SELECT id
            from contacts
            where email = :email";
        $stmt = $this->db->prepare($sql);

        $stmt->execute(["email" => $email]);
		$stmt->fetch();

		// If we don't have any matches return false
        if($stmt->rowCount(["email" => $email]) == 0){
        	return FALSE;
        } 

        return TRUE;

    }
    
    //Save the data to the data file
    public function save($params)
    {
    	// Very simple insert statement.
        $sql = "insert into contacts
            (name, email, tel, dob) values
            (:name, :email, :tel, :dob)";
        $stmt = $this->db->prepare($sql);

        $result = $stmt->execute($params);

        // Must add error handling
        
    }

}